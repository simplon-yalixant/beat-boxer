using UnityEngine;

public class RythmSpawn : MonoBehaviour
{
    public AudioSource audioSource;
    public float threshold = 0.5f;
    public float spawnDelay = 0.1f;
    public float peakMultiplier = 100.0f;
    public int numSamples = 1024;

    private float[] spectrumData;
    private float currentMaxValue = 0.0f;
    private float lastMaxValue = 0.0f;
    private float deltaMaxValue = 0.0f;

    void Start()
    {
        spectrumData = new float[numSamples];
        audioSource.Play();
    }

    void Update()
    {
        audioSource.GetSpectrumData(spectrumData, 0, FFTWindow.BlackmanHarris);

        // Calcule la valeur maximale de la fr�quence de cr�te (beat)
        currentMaxValue = 0f;
        int currentMaxIndex = 0;
        for (int i = 0; i < numSamples; i++)
        {
            if (spectrumData[i] > currentMaxValue)
            {
                currentMaxValue = spectrumData[i];
                currentMaxIndex = i;
            }
        }
        currentMaxValue *= peakMultiplier;

        deltaMaxValue = currentMaxValue - lastMaxValue;

        if (deltaMaxValue > threshold)
        {
            Debug.Log("arthur");
        }

        lastMaxValue = currentMaxValue;
    }
}
